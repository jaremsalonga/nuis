let mongoose = require('mongoose')
let Schema = mongoose.Schema
let ObjectID = mongoose.ObjectID


var userSchema = new Schema({
    firstname:String,
    middlename:String,
    lastname:String,
    role:String,
    created:String
})

module.exports = mongoose.model('Users',userSchema);