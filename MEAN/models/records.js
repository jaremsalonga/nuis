var mongoose = require('mongoose'),
Schema = mongoose.Schema

var recordSchema = new Schema({
    address:String,
    age:Number,
    birthday:Date,
    mothers_name:String,
    fathers_name:String

})

module.exports = mongoose.model('RecordSchema',recordSchema);