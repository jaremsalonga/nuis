let express = require('express');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let path = require('path');
let ejs = require('ejs');
let engine = require('ejs-mate');
let session = require('express-session');
let mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/testDB');
let app = express();
let Port = process.env.PORT || 3000

app.engine('ejs',engine);
app.set('view engine','ejs');

app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));

app.use(cookieParser());

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());


app.get('/', (req,res) => {
    res.render('index')
})

app.get('/sign-up', (req,res) => {
    res.render('index')
})

app.listen(Port, (a) => {
    console.log(`App Listening on ${Port}`)
})