let app = angular.module("myApp", []);

app.controller("HomeController", ($scope) => {
        $scope.changeClass = () =>{
                if($scope.burgerClass == "is-open")
                $scope.burgerClass = "is-closed";
                else
                $scope.burgerClass = "is-open"
        }
        $scope.openModal = () => {
                console.log($scope);
        }
})

app.directive('modalTest', () => {
        return {
                restrict:'E',
                templateUrl:'/views/partials/modal-test.html'
        }
})